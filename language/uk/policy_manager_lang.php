<?php

$lang['policy_manager_allusers_warning'] = 'Група allusers — це спеціальна група, яка використовується для відстеження всіх користувачів у системі. Будь ласка, створіть нову групу, якщо вам потрібно вказати підмножину користувачів у системі.';
$lang['policy_manager_app_description'] = 'Доповнення «Диспетчер політик» надає механізм для розгортання певних політик додатків у певних групах.';
$lang['policy_manager_app_invalid'] = 'Додаток недійсний.';
$lang['policy_manager_app_name'] = 'Менеджер політики';
$lang['policy_manager_description_invalid'] = 'Опис недійсний.';
$lang['policy_manager_group'] = 'Група';
$lang['policy_manager_group_invalid'] = 'Група недійсна.';
$lang['policy_manager_policies'] = 'Політики';
$lang['policy_manager_policy'] = 'Політика';
$lang['policy_manager_policy_already_exists'] = 'Политика уже существует.';
$lang['policy_manager_policy_id_invalid'] = 'ID політики недійсний.';
$lang['policy_manager_policy_name'] = 'Назва політики';
$lang['policy_manager_policy_name_invalid'] = 'Назва політики недійсна.';
$lang['policy_manager_policy_not_found'] = 'Політику не знайдено.';
$lang['policy_manager_priority_invalid'] = 'Пріоритет недійсний.';
